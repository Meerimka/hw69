import React, {Component} from 'react';
import  axios from '../../axios-products';
import {connect} from 'react-redux';
import './ContactData.css';
import Button from "../../components/UI/Button/Button";
import {addProducts} from "../../store/actions";





class ContactData extends Component {
    state={
        name:'',
        email:'',
        street:'',
        postal:'',

    };

    valueChanged = event =>{

        const{name,value} = event.target;

        this.setState({[name]: value});
    };

    orderHandler=event=>{
        event.preventDefault();
        const user = this.state;
        this.props.addProducts(user)

    };
    render() {
        let div = (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                <form onSubmit={this.orderHandler}>
                    <input className="Input" type="text" name="name" placeholder="Your Name"
                        value={this.state.name} onChange={this.valueChanged}
                    />
                    <input className="Input" type="email" name="email" placeholder="Your Mail"
                           value={this.state.email} onChange={this.valueChanged}
                    />
                    <input className="Input" type="text" name="street" placeholder="Street"
                           value={this.state.street} onChange={this.valueChanged}
                    />
                    <input className="Input" type="text" name="postal" placeholder="Postal Code"
                           value={this.state.postal} onChange={this.valueChanged}
                    />
                    <Button btnType="Success" onClick ={this.props.close}>ORDER</Button>
                </form>
            </div>
        );
        return div;
    }
}

const mapStateToProps = state=>{
    return{
        cart: state.add.cart,
        totalPrice: state.add.totalPrice,
    }
};
const mapDispatchToProps = dispatch =>{
    return{
        addProducts: (user)=> dispatch(addProducts(user)),
    }
}

export default connect (mapStateToProps,mapDispatchToProps)(ContactData);