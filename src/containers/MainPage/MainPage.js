import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import "../../store/actions";
import {addProd, deleteProd, fetchProd, purchasable, purchaseCancel} from "../../store/actions";
import './MainPage.css';
import Modal from "../../components/UI/Modal/Modal";
import ContactData from "../ContactData/ContactData";


class MainPage extends Component {
    componentDidMount(){
        this.props.fetchProd();
    }

    render() {
        return (
            <div className="Prod">
                <div  className="Menu">
                {Object.keys(this.props.menu).map((food, index)=>{
                    return(
                        <div key={index}  className="Food">
                            <span className="food-name">{food}</span>
                            <img className="img" src={this.props.menu[food].image} alt=""/>
                            <span>{this.props.menu[food].price} KGS</span>
                            <button className="btn" onClick={()=>this.props.addProd({name:food, price: this.props.menu[food].price})}>Add to card</button>
                        </div>
                    )
                })}
                </div>
                <div className="Cart">
                {Object.keys(this.props.cart).map((food,index)=>{
                    return(
                        this.props.cart[food].amount === 0 ? null : <div key={index} >
                            <span>{food} {' '}</span>
                            <span>{" "} X {this.props.cart[food].amount}</span>
                            <button onClick={()=>this.props.deleteProd({name:food, price: this.props.menu[food].price})}>X</button>
                        </div>
                    )
                })}
                    <p>food delivery 150 KGS </p>
                    <p className="Total">Total price equal to : {this.props.totalPrice} KGS</p>
                    {this.props.totalPrice > 150 ? <button
                        className="btn"
                        onClick={this.props.makeOrder}
                    >
                        ORDER NOW
                    </button> : null}
                    <Modal
                        show={this.props.purchasing}
                        close={this.props.purchaseCancel}
                    >
                        <ContactData
                            close={this.props.purchaseCancel}
                        />
                    </Modal>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state=>{
    return{
        menu: state.menu.menu,
        cart: state.add.cart,
        totalPrice: state.add.totalPrice,
        purchasing: state.add.purchasing
    }
    }


const mapDispatchToProps = dispatch =>{
    return{
        addProd: (amount) =>dispatch(addProd(amount)),
        deleteProd: (amount) =>dispatch(deleteProd(amount)),
        fetchProd : () => dispatch(fetchProd()),
        makeOrder: () => dispatch(purchasable()),
        purchaseCancel: () =>dispatch(purchaseCancel()),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);