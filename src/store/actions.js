import axios from '../axios-products';

export  const ADD ='ADD';
export  const DELETE ='DELETE';
export const  PURCHASABLE = 'PURCHASABLE';
export const PURCHASECANCEL ='PURCHASECANCEL'

export const FETCH_PROD_REQUEST = 'FETCH_PROD_REQUEST';
export const FETCH_PROD_SUCCESS = 'FETCH_PROD_SUCCESS';
export const FETCH_PROD_ERROR = 'FETCH_PROD_ERROR';

export const purchasable = () =>{
        return {type:PURCHASABLE}
};

export const purchaseCancel = () =>{
    return dispatch=>{
        dispatch({type:PURCHASECANCEL})
    }
};

export const addProd= amount=>{
    return dispatch => {
        dispatch({type: ADD, amount});
    }
};

export const deleteProd= amount=>{
    return dispatch => {
        dispatch({type: DELETE, amount});
    }
};

export const fetchProdRequest = () =>{
    return {type: FETCH_PROD_REQUEST}
};

export const fetchProdSuccess = prod =>{
    return {type: FETCH_PROD_SUCCESS, prod}
};

export const fetchProdError = error =>{
    return {type: FETCH_PROD_ERROR, error}
};


export const fetchProd = () =>{
    return dispatch =>{
        dispatch(fetchProdRequest());
        axios.get('products.json').then(response =>{
            dispatch(fetchProdSuccess(response.data));
        },error =>{
            dispatch(fetchProdError());
        });
    }
};

export const addProducts = user =>{
    return (dispatch, getState) => {
        const order = {
            user: user,
            order: getState().cart
        };
        return axios.post('orders.json', order)
    }
};
