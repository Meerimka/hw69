import {
    ADD, DELETE, FETCH_PROD_ERROR,
    FETCH_PROD_REQUEST,
    FETCH_PROD_SUCCESS, PURCHASABLE, PURCHASECANCEL,
} from "./actions";


const initialState={
    menu: {},
};

const reducerAdd = (state= initialState , action) => {
    switch (action.type) {
        case FETCH_PROD_REQUEST:
            return{
                ...state,
                loading: true,
            };

        case FETCH_PROD_SUCCESS:
            return{
                ...state,
                menu: action.prod,
                loading:false,
            };
        case FETCH_PROD_ERROR:
            return{
                ...state,
                loading:true,
            };
        default:
            return state;
    }


};

export default reducerAdd;