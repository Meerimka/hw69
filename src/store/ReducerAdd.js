import {
    ADD,
    DELETE,
    PURCHASABLE,
    PURCHASECANCEL
} from "./actions";

const initialState={
    cart: [],
    totalPrice: 150,
    purchasing: false,
};

const reducerMenu = (state=initialState , action) => {
    switch (action.type) {
        case ADD:
            let order;
            if(state.cart[action.amount.name]) {
                order = {...action.amount, amount: state.cart[action.amount.name].amount + 1};
            } else {
                order = {...action.amount, amount: 1};
            }
            const amount = {...state.cart, [action.amount.name]: order};
            return{
                ...state,
                cart: amount,
                totalPrice: state.totalPrice + action.amount.price
            };
        case DELETE:
            if(state.cart[action.amount.name]) {
                order = {...action.amount, amount: state.cart[action.amount.name].amount -1, };
            }
            const  food = {...state.cart, [action.amount.name]: order};
            return{
                ...state,
                cart: food,
                totalPrice: state.totalPrice - action.amount.price,
            };
        case PURCHASABLE:
            return{
                ...state,
                purchasing: true
            };
        case PURCHASECANCEL:
            return{
                ...state,
                purchasing: false,
                cart: [],
                totalPrice: 150,
            };

        default:
            return state;
    }


};

export default reducerMenu;