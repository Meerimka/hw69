import React, { Component } from 'react';
import {Route,Switch} from 'react-router-dom';
import './App.css';
import MainPage from "./containers/MainPage/MainPage";

class App extends Component {
    render() {
        return (
                <Switch>
                    <Route path="/" exact component={MainPage}/>
                </Switch>
        );
    }
}

export default App;