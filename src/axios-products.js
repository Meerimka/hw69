import axios from 'axios';

const instance =axios.create({
    baseURL: 'https://products-junusova.firebaseio.com/',
});

export default instance;